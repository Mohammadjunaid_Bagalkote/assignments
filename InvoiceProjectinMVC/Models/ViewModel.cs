﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InvoiceProjectinMVC.Models
{
    public class ViewModel
    {
        public Invoice_List model1 { get; set; }
        public Invoice_Details model2 { get; set; }
    }
}